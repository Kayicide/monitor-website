﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise;
using LogsMonitor.Models.Login;
using LogsMonitor.Utility;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Pages.Login
{
    public partial class Login : ComponentBase
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Inject]
        protected AuthenticationStateProvider AuthenticationStateProvider { get; set; }
        [Inject]
        protected NavigationManager NavigationManager { get; set; }
        [Inject]
        protected Blazored.SessionStorage.ISessionStorageService SessionStorage { get; set; }

        [Parameter]
        public string RedirectUrl { get; set; }

        private LoginUser _user = new ();
        private bool _rememberMe = false;
        private string _error;

        protected override async Task OnInitializedAsync()
        {
            //var authState = await AuthenticationStateTask;
            //if (authState?.User.Identity != null || authState.User.Identity.IsAuthenticated)
            //{
            //    var userId = authState.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            //    NavigationManager.NavigateTo($"/account", true);
            //}
        }

        private async Task SubmitAsync()
        { 

            var result = await ((CustomAuthenticationStateProvider)AuthenticationStateProvider).UpdateAuthState(_user.Email, _user.Password, _rememberMe);
            if(result.Success)
                NavigationManager.NavigateTo(!string.IsNullOrEmpty(RedirectUrl) ? RedirectUrl : "/", true);

            _error = result.ErrorMessage;
        }
    }
}
