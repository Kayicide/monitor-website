﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.UserFeature;
using LogsMonitor.Models.Register;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Pages.Register
{
    public partial class Register : ComponentBase
    {
        [Inject]
        protected NavigationManager NavigationManager { get; set; }
        [Inject]
        protected IRequestHandler<RegisterUserRequest, User> RegisterUserRequestHandler { get; set; }

        private RegisterUser _registerUser;
        private string _error;

        protected override Task OnInitializedAsync()
        {
            _registerUser = new();
            return base.OnInitializedAsync();
        }

        private async Task SubmitAsync()
        {
            var user = await RegisterUserRequestHandler.HandleRequest(new RegisterUserRequest
            {
                Email = _registerUser.Email,
                FirstName = _registerUser.FirstName,
                LastName = _registerUser.LastName,
                Password = _registerUser.Password
            });

            if (user == null)
                _error = "Email is already in use!";
            else
                NavigationManager.NavigateTo("/Login");
        }

        private bool CheckAllPopulated()
        {
            if (string.IsNullOrEmpty(_registerUser.Email))
                return false;
            if (string.IsNullOrEmpty(_registerUser.FirstName))
                return false;
            if (string.IsNullOrEmpty(_registerUser.LastName))
                return false;
            if (string.IsNullOrEmpty(_registerUser.Password))
                return false;
            if (string.IsNullOrEmpty(_registerUser.ConfirmPassword))
                return false;
            return true;
        }
    }
}
