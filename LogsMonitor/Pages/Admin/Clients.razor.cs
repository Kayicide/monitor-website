﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise.DataGrid;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.UserFeature;
using LogsMonitor.Models.Admin;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Pages.Admin
{
    public partial class Clients
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Inject] protected IRequestHandler<GetAllClientsRequest, IList<Data.Entity.Client>> GetAllClientsRequestHandler { get; set; }
        [Inject] private IRequestHandler<UpdateClientNameRequest, Data.Entity.Client> UpdateClientNameRequestHandler { get; set; }

        private IList<Data.Entity.Client> _clients;
        private string _currentUser;
        protected override async Task OnInitializedAsync()
        {
            _clients = await GetAllClientsRequestHandler.HandleRequest(new GetAllClientsRequest());
            _currentUser = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        }

        protected async Task OnRowUpdatedAsync(SavedRowItem<Data.Entity.Client, Dictionary<string, object>> e)
        {
            var client = e.Item;
            await UpdateClientNameRequestHandler.HandleRequest(new UpdateClientNameRequest
            {
                CurrentUser = _currentUser,
                ClientId = client.Id,
                Name = client.Name,
                Active = client.Active
            });
        }
    }
}
