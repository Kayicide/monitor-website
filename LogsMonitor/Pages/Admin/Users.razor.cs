﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazorise.DataGrid;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.UserFeature;
using Microsoft.AspNetCore.Components;

namespace LogsMonitor.Pages.Admin
{
    public partial class Users
    {
        [Inject] protected IRequestHandler<GetAllUsersRequest, IList<User>> GetAllUsersRequestHandler { get; set; }
        [Inject] protected IRequestHandler<UpdateUserRequest, User> UpdateUserRequestHandler { get; set; }
        private IList<User> _users;
        //visual studio doesn't like this class but it does compile and work's correctly.
        protected override async Task OnInitializedAsync()
        {
            _users = await GetAllUsersRequestHandler.HandleRequest(new GetAllUsersRequest());
        }
        protected async Task OnRowUpdatedAsync(SavedRowItem<User, Dictionary<string, object>> e)
        {
            var user = e.Item;
            await UpdateUserRequestHandler.HandleRequest(new UpdateUserRequest
            {
                UserId = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Role = user.Role,
                Deleted = user.Deleted
            });
        }
    }
}
