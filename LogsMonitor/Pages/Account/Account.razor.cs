﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.UserFeature;
using LogsMonitor.Models.Account;
using LogsMonitor.Utility;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Pages.Account
{
    public partial class Account : ComponentBase
    {
        [CascadingParameter]
        private Task<AuthenticationState> AuthenticationStateTask { get; set; }

        [Inject] private IRequestHandler<GetUserRequest, User> GetUserRequestHandler { get; set; }
        [Inject] private IRequestHandler<UpdateUserRequest, User> UpdateUserRequestHandler { get; set; }

        private readonly AccountModel _accountModel = new();
        private User _user = new();
        private string _error;
        private bool _accountUpdateSuccess;
        private bool _passwordUpdateSuccess;
        protected override async Task OnInitializedAsync()
        {
            var userId = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            _user = await GetUserRequestHandler.HandleRequest(new GetUserRequest {UserId = userId});
            UpdateModel(_user);
        }

        private async Task SubmitAsync()
        {
            _accountUpdateSuccess = false;
            _passwordUpdateSuccess = false;
            var request = new UpdateUserRequest
            {
                FirstName = _accountModel.FirstName,
                LastName = _accountModel.LastName,
                UserId = _user.Id
            };

            if (!string.IsNullOrEmpty(_accountModel.NewPassword))
            {
                var userId = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                var currentUser = await GetUserRequestHandler.HandleRequest(new GetUserRequest { UserId = userId });
                request.Password = CheckPassword(currentUser);
            }

            var newUser = await UpdateUserRequestHandler.HandleRequest(request);
            _accountUpdateSuccess = true;
            UpdateModel(newUser);
        }

        private string CheckPassword(User currentUser)
        {
            var saltBytes = Convert.FromBase64String(currentUser.HashSalt.Salt);
            var encrypted = new Rfc2898DeriveBytes(_accountModel.CurrentPassword, saltBytes, 10000);
            if (Convert.ToBase64String(encrypted.GetBytes(256)) == currentUser.HashSalt.Hash)
            {
                ClearPasswordFields();
                _passwordUpdateSuccess = true;
                return _accountModel.NewPassword;
            }

            _error = "Password has not been updated: Current password is incorrect!";
            return string.Empty;
        }
        private void UpdateModel(User user)
        {
            _accountModel.Email = user.Email;
            _accountModel.FirstName = user.FirstName;
            _accountModel.LastName = user.LastName;
        }

        private void ClearPasswordFields()
        {
            _accountModel.NewPassword = string.Empty;
            _accountModel.ConfirmNewPassword = string.Empty;
            _accountModel.CurrentPassword = string.Empty;
        }
    }
}
