﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.Shared;
using LogsMonitor.Features.UserFeature;
using LogsMonitor.Models.Client;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.JSInterop;

namespace LogsMonitor.Pages.Client
{
    public partial class ClientPage : ComponentBase
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Parameter] public string ClientId { get; set; }
        [Inject] protected NavigationManager NavigationManager { get; set; }
        [Inject] private IRequestHandler<GetClientRequest, Data.Entity.Client> GetClientRequestHandler { get; set; }
        [Inject] private IRequestHandler<GetAllUsersRequest, IList<User>> GetAllUsersRequestHandler { get; set; }
        [Inject] private IRequestHandler<UpdateClientNameRequest, Data.Entity.Client> UpdateClientNameRequestHandler { get; set;}
        [Inject] private IRequestHandler<RegenJoinTokenRequest, Data.Entity.Client> RegenJoinTokenRequestHandler { get; set; }
        [Inject] private IRequestHandler<GenerateJwtTokenRequest, Data.Entity.Client> GenJwtTokenRequestHandler { get; set; }
        [Inject] private IRequestHandler<LeaveClientRequest, bool> LeaveClientRequestHandler { get; set; }
        [Inject] private IRequestHandler<AddAdminRequest, Data.Entity.Client> AddAdminRequestHandler { get; set; }
        [Inject] private IRequestHandler<RemoveAdminRequest, Data.Entity.Client> RemoveAdminRequestHandler { get; set; }
        [Inject] private IRequestHandler<TransferOwnershipRequest, Data.Entity.Client> TransferOwnershipRequestHandler { get; set; }

        private Modal refreshJwtModalRef;
        private Modal leaveTeamModalRef;
        private Modal transferOwnershipModalRef;
        private Modal deactivateClientModalRef;
        private readonly ClientModel _clientModel = new();
        private string _copyJwtTokenToClipboardClass = "fas fa-clipboard";
        private string _copyJoinTokenToClipboardClass = "fas fa-clipboard";
        private string _currentUser;
        private string _userToPromote = string.Empty;

        protected override async Task OnParametersSetAsync()
        {
            await GetData();
        }

        private async Task GetData()
        {

            var client = await GetClientRequestHandler.HandleRequest(new GetClientRequest(ClientId));
            var members = await GetAllUsersRequestHandler.HandleRequest(new GetAllUsersRequest { ClientId = client.Id });
            _currentUser = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;

            if (!members.Any(x => x.Id.Equals(_currentUser, StringComparison.InvariantCultureIgnoreCase)))
                NavigationManager.NavigateTo("/");

            _clientModel.Client = client;
            _clientModel.Members = members;
            if (_clientModel.Client.Admins.Contains(_currentUser, StringComparer.InvariantCultureIgnoreCase))
                _clientModel.Admin = true;
        }

        public async Task UpdateClientName()
        {
            var updatedClient = await UpdateClientNameRequestHandler.HandleRequest(new UpdateClientNameRequest{ClientId = _clientModel.Client.Id, Name = _clientModel.Client.Name, CurrentUser = _currentUser});
            if(updatedClient == null)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            _clientModel.Client = updatedClient;
        }

        public async Task LeaveClient(string userId)
        {
            var result = await LeaveClientRequestHandler.HandleRequest(new LeaveClientRequest
                {CurrentUser = _currentUser, UserId = userId, ClientId = _clientModel.Client.Id});
            if(result == false)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            if(userId.Equals(_currentUser, StringComparison.InvariantCultureIgnoreCase))
                NavigationManager.NavigateTo($"/", true);

            _clientModel.Members = await GetAllUsersRequestHandler.HandleRequest(new GetAllUsersRequest { ClientId = _clientModel.Client.Id });
        }

        public async Task RefreshJoinToken()
        {
            var updatedClient = await RegenJoinTokenRequestHandler.HandleRequest(new RegenJoinTokenRequest{ ClientId = _clientModel.Client.Id, CurrentUser = _currentUser});
            if (updatedClient == null)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            _clientModel.Client = updatedClient;
        }

        public async Task RefreshJwtToken()
        {
            var updatedClient = await GenJwtTokenRequestHandler.HandleRequest(new GenerateJwtTokenRequest{ ClientId = _clientModel.Client.Id, CurrentUser = _currentUser });
            if (updatedClient == null)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            _clientModel.Client = updatedClient;

            HideModal(refreshJwtModalRef);
        }

        public async Task PromoteUser(string userId)
        {
            var updatedClient = await AddAdminRequestHandler.HandleRequest(new AddAdminRequest{ ClientId = _clientModel.Client.Id, CurrentUser = _currentUser, UserId = userId });
            if (updatedClient == null)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            _clientModel.Client = updatedClient;
        }

        public async Task DemoteUser(string userId)
        {
            var updatedClient = await RemoveAdminRequestHandler.HandleRequest(new RemoveAdminRequest { ClientId = _clientModel.Client.Id, CurrentUser = _currentUser, UserId = userId });
            if (updatedClient == null)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            _clientModel.Client = updatedClient;
        }

        public void PromoteUserToOwner(string userId)
        {
            _userToPromote = userId;
            ShowModal(transferOwnershipModalRef);
        }

        public async Task ConfirmPromote()
        {
            if (_userToPromote != string.Empty)
            {
                HideModal(transferOwnershipModalRef);
                var updatedClient = await TransferOwnershipRequestHandler.HandleRequest(new TransferOwnershipRequest { ClientId = _clientModel.Client.Id, CurrentUser = _currentUser, UserId = _userToPromote });
                if (updatedClient == null)
                    NavigationManager.NavigateTo($"/Team/{ClientId}", true);
                _clientModel.Client = updatedClient;
            }
        }

        public async Task DeleteTeam()
        {
            var result = await UpdateClientNameRequestHandler.HandleRequest(new UpdateClientNameRequest { ClientId = _clientModel.Client.Id, CurrentUser = _currentUser, Active = false});
            if (result == null)
                NavigationManager.NavigateTo($"/Team/{ClientId}", true);
            NavigationManager.NavigateTo($"/", true);
        }

        private async Task CopyJwtTokenToClipboard()
        {
            _copyJwtTokenToClipboardClass = "fas fa-clipboard-check";
            await JS.InvokeVoidAsync("navigator.clipboard.writeText", _clientModel.Client.ApiToken);
        }
        private async Task CopyJoinTokenToClipboard()
        {
            _copyJoinTokenToClipboardClass = "fas fa-clipboard-check";
            await JS.InvokeVoidAsync("navigator.clipboard.writeText", _clientModel.Client.JoinToken);
        }
        private void ShowModal(Modal modalRef)
        {
            modalRef.Show();
        }

        private void HideModal(Modal modalRef)
        {
            modalRef.Hide();
        }

        public bool DisallowLeave()
        {
            if (_currentUser != null && _clientModel.Client != null)
                return _currentUser.Equals(_clientModel.Client.Owner, StringComparison.InvariantCultureIgnoreCase);
            return false;
        }

        public bool CurrentUserOrAdmin(string userId)
        {
            if(_clientModel.Client != null)
                return _currentUser.Equals(userId, StringComparison.InvariantCultureIgnoreCase) || _clientModel.Client.Admins.Contains(userId) && !_currentUser.Equals(_clientModel.Client.Owner, StringComparison.InvariantCultureIgnoreCase);
            return true;
        }
    }
}
