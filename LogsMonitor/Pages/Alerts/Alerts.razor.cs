﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazorise.DataGrid;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.AlertFeature;
using Microsoft.AspNetCore.Components;

namespace LogsMonitor.Pages.Alerts
{
    public partial class Alerts
    {
        [Parameter] public string ClientId { get; set; }
        [Inject] public IRequestHandler<GetAllAlertsRequest, IList<Alert>> GetAllAlertsRequestHandler { get; set; }
        [Inject] public IRequestHandler<CreateNewAlertRequest, Alert> CreateNewAlertRequestHandler { get; set; }
        [Inject] public IRequestHandler<UpdateAlertRequest, Alert> UpdateAlertRequestHandler { get; set; }
        private IList<Alert> _alerts;
        //visual studio doesn't like this class but it does compile and work's correctly.
        protected override async Task OnInitializedAsync()
        {
            _alerts = await GetAllAlertsRequestHandler.HandleRequest(new GetAllAlertsRequest {ClientId = ClientId});
        }

        protected async Task OnRowUpdatedAsync(SavedRowItem<Alert, Dictionary<string, object>> e)
        {
            var alert = e.Item;
            await UpdateAlertRequestHandler.HandleRequest(new UpdateAlertRequest
            {
                AlertId = alert.Id,
                Name = alert.Name,
                LogType = alert.LogType,
                LogIdentifier = alert.LogIdentifier,
                TimeSpan = alert.TimeSpan,
                ThresholdAmount = alert.ThresholdAmount
            });
        }
        protected async Task OnRowInsertedAsync(SavedRowItem<Alert, Dictionary<string, object>> e)
        {
            var alert = e.Item;
            await CreateNewAlertRequestHandler.HandleRequest(new CreateNewAlertRequest
            {
                ClientId = ClientId,
                Name = alert.Name,
                LogType = alert.LogType,
                LogIdentifier = alert.LogIdentifier,
                TimeSpan = alert.TimeSpan,
                ThresholdAmount = alert.ThresholdAmount
            });
        }
        protected async Task OnRowDeletedAsync(Alert deletedAlert)
        {
            await UpdateAlertRequestHandler.HandleRequest(new UpdateAlertRequest
            {
                AlertId = deletedAlert.Id,
                Name = deletedAlert.Name,
                LogType = deletedAlert.LogType,
                LogIdentifier = deletedAlert.LogIdentifier,
                TimeSpan = deletedAlert.TimeSpan,
                ThresholdAmount = deletedAlert.ThresholdAmount,
                Deleted = true
            });
        }

        void OnAlertNewItemDefaultSetter(Alert alert)
        {
            alert.TimeSpan = 10;
            alert.ThresholdAmount = 10;
            alert.LogType = "Any";
        }
    }
}
