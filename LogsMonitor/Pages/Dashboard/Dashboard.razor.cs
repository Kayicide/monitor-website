﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Blazorise;
using Blazorise.Charts;
using LoggingService.Features;
using LogsMonitor.Components.Dashboard;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.DashboardFeature;
using LogsMonitor.Features.LogFeature;
using LogsMonitor.Models.Dashboard;
using LogsMonitor.Utility.Mappers;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Bson;

namespace LogsMonitor.Pages.Dashboard
{
    public partial class Dashboard : IAsyncDisposable
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Parameter] public string ClientId { get; set; }
        [Parameter] public string DashboardId { get; set; }
        [Inject] public IMapper<Data.Entity.Dashboard, DashboardModel> DashboardMapper { get; set; }
        [Inject] public IRequestHandler<GetAllLogsRequest, IList<Log>> GetAllLogsRequestHandler { get; set; }
        [Inject] public IRequestHandler<GetDashboardRequest, Data.Entity.Dashboard> GetDashboardRequestHandler { get; set; }
        [Inject] public IRequestHandler<UpdateDashboardRequest, Data.Entity.Dashboard> UpdateDashboardRequestHandler { get; set; }
        [Inject] private IRequestHandler<GetClientRequest, Data.Entity.Client> GetClientRequestHandler { get; set; }

        private Modal _settingModalRef;
        private Modal _alertModal;
        private DashboardModel _dashboard;
        private LineGraph _lineGraph = new();
        private Timer _timer;

        protected override Task OnInitializedAsync()
        {
            RefreshLoop();
            return base.OnInitializedAsync();
        }

        protected override async Task OnParametersSetAsync()
        {
            await GetData();
        }

        public async Task GetData()
        {
            _dashboard = DashboardMapper.Map(await GetDashboardRequestHandler.HandleRequest(new GetDashboardRequest { DashboardId = DashboardId }));

            var currentUser = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            var client = await GetClientRequestHandler.HandleRequest(new GetClientRequest(ClientId));
            if (client.Admins.Contains(currentUser) || client.Owner.Equals(currentUser, StringComparison.CurrentCultureIgnoreCase))
                _dashboard.IsAdminOwner = true;
        }

        public async Task SaveSettings()
        {
            HideModal(_settingModalRef);
            await UpdateDashboardRequestHandler.HandleRequest(new UpdateDashboardRequest
            {
                Id = _dashboard.Id,
                ShowTotalLogs = _dashboard.ShowTotalLogs,
                TimeScale = _dashboard.TimeScale,
                Name = _dashboard.Name,
                ShowErrors = _dashboard.ShowErrors,
                ShowInformation = _dashboard.ShowInformation,
                ShowWarnings = _dashboard.ShowWarnings,
                LogIdentifier = _dashboard.LogIdentifier,
                LogIdentifierExact = _dashboard.LogIdentifierExact,
                NumberToTrigger = _dashboard.NumberToTrigger,
                Popup = _dashboard.Popup
            });
            await _lineGraph.HandleRedraw();
        }

        public void RefreshLoop()
        {
            var startTimeSpan = TimeSpan.FromSeconds(60 - DateTime.Now.Second);
            var periodTimeSpan = TimeSpan.FromMinutes(1);

            _timer = new Timer(async (e) =>
            {
                await Update(DateTime.Now);
            }, null, startTimeSpan, periodTimeSpan);
        }

        public async Task Update(DateTime dateTime)
        {
            var lowerDate = GetLowerDate(dateTime);
            var newLogs = await GetAllLogsRequestHandler.HandleRequest(new GetAllLogsRequest { ClientId = ClientId, LowerBoundDateTime = lowerDate, IdentifierExact = _dashboard.LogIdentifierExact, Identifier = _dashboard.LogIdentifier});

            if (_dashboard.Popup)
                ManagePopup(newLogs);

            await _lineGraph.Refresh(dateTime, newLogs);
        }
        public DateTime GetLowerDate(DateTime dateTime)
        {
            switch (_dashboard.TimeScale)
            {
                case XAxisTimeScale.Day:
                    return dateTime.AddHours(-dateTime.Hour);
                case XAxisTimeScale.Minute:
                    return dateTime.AddSeconds(-dateTime.Second).AddMinutes(-1);
                default:
                    return dateTime.AddMinutes(-dateTime.Minute);
            }
        }
        private void ShowModal(Modal modalRef)
        {
            modalRef.Show();
        }

        private void HideModal(Modal modalRef)
        {
            modalRef.Hide();
        }

        public ValueTask DisposeAsync()
        {
            _timer.DisposeAsync();
            return default;
        }

        private void ManagePopup(IList<Log> logs)
        {
            if (logs.Count >= _dashboard.NumberToTrigger && _alertModal.Visible == false)
            {
                _dashboard.NumberActually = logs.Count;
                ShowModal(_alertModal);
            }
        }

        public void UpdateTriggerNumber(int value )
        {
            _dashboard.NumberToTrigger = value;
        }
    }
}