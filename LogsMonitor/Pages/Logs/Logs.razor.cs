﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.LogFeature;
using LogsMonitor.Models.Logs;
using Microsoft.AspNetCore.Components;

namespace LogsMonitor.Pages.Logs
{
    public partial class Logs : ComponentBase
    {
        [Parameter] public string ClientId { get; set; }
        [Inject] protected NavigationManager NavigationManager { get; set; }
        [Inject] public IRequestHandler<GetAllLogsRequest, IList<Log>> GetAllLogsRequestHandler { get; set; }
        [Inject] public IRequestHandler<GetAllErrorLogsRequest, IList<Error>> GetAllErrorLogsRequestHandler { get;set; }
        [Inject] public IRequestHandler<GetAllInformationLogsRequest, IList<Information>> GetAllInformationLogsRequestHandler { get; set; }
        [Inject] public IRequestHandler<GetAllWarningLogsRequest, IList<Warning>> GetAllWarningLogsRequestHandler { get; set; }

        private LogsModel _logsModel = new();
        protected override async Task OnInitializedAsync()
        {
            await GetLogs();
            OrderByDesc();
        }

        public async Task ResetFilter()
        {
            _logsModel = new();
            await GetLogs();

        }
        public async Task Filter()
        {
            switch (_logsModel.LogType)
            {
                case 1:
                    await FilterAll();
                    break;
                case 2:
                    await FilterError();
                    break;
                case 3:
                    await FilterInformation();
                    break;
                case 4:
                    await FilterWarning();
                    break;
            }
        }


        public async Task FilterAll()
        {
            _logsModel.Logs = _logsModel.Logs = (await GetAllLogsRequestHandler.HandleRequest(new GetAllLogsRequest
            {
                ClientId = ClientId,
                UpperBoundDateTime = _logsModel.UpperBoundDateTime,
                LowerBoundDateTime = _logsModel.LowerBoundDateTime,
                Summary = _logsModel.Summary,
                SummaryExact = _logsModel.SummaryExact,
                Identifier = _logsModel.Identifier,
                IdentifierExact = _logsModel.IdentifierExact
            })).ToList();
            OrderByDesc();
        }

        public async Task FilterError()
        {
            _logsModel.Errors = (await GetAllErrorLogsRequestHandler.HandleRequest(new GetAllErrorLogsRequest
            {
                ClientId = ClientId,
                UpperBoundDateTime = _logsModel.UpperBoundDateTime,
                LowerBoundDateTime = _logsModel.LowerBoundDateTime,
                Summary = _logsModel.Summary,
                SummaryExact = _logsModel.SummaryExact,
                Identifier = _logsModel.Identifier,
                IdentifierExact = _logsModel.IdentifierExact,
                StackTrace = _logsModel.StackTrace,
                StackTraceExact = _logsModel.StackTraceExact,
                Severity = _logsModel.Severity
            })).ToList();
            _logsModel.InformationLogs = new List<Information>();
            _logsModel.WarningLogs = new List<Warning>();
            _logsModel.Logs = _logsModel.InformationLogs.Concat(_logsModel.Errors.Cast<Log>().Concat(_logsModel.WarningLogs.Cast<Log>())).ToList();
            OrderByDesc();
        }

        public async Task FilterInformation()
        {
            _logsModel.Errors = new List<Error>();
            _logsModel.InformationLogs = (await GetAllInformationLogsRequestHandler.HandleRequest(new GetAllInformationLogsRequest
            {
                ClientId = ClientId,
                UpperBoundDateTime = _logsModel.UpperBoundDateTime,
                LowerBoundDateTime = _logsModel.LowerBoundDateTime,
                Summary = _logsModel.Summary,
                SummaryExact = _logsModel.SummaryExact,
                Identifier = _logsModel.Identifier,
                IdentifierExact = _logsModel.IdentifierExact,
                InfoText = _logsModel.InfoText,
                InfoTextExact = _logsModel.InfoTextExact
            })).ToList();
            _logsModel.WarningLogs = new List<Warning>();
            _logsModel.Logs = _logsModel.InformationLogs.Concat(_logsModel.Errors.Cast<Log>().Concat(_logsModel.WarningLogs.Cast<Log>())).ToList();
            OrderByDesc();
        }

        public async Task FilterWarning()
        {
            _logsModel.Errors = new List<Error>();
            _logsModel.InformationLogs = new List<Information>();
            _logsModel.WarningLogs = (await GetAllWarningLogsRequestHandler.HandleRequest(new GetAllWarningLogsRequest
            {
                ClientId = ClientId,
                UpperBoundDateTime = _logsModel.UpperBoundDateTime,
                LowerBoundDateTime = _logsModel.LowerBoundDateTime,
                Summary = _logsModel.Summary,
                SummaryExact = _logsModel.SummaryExact,
                Identifier = _logsModel.Identifier,
                IdentifierExact = _logsModel.IdentifierExact,
                WarningText = _logsModel.WarningText,
                WarningTextExact = _logsModel.WarningTextExact
            })).ToList();
            _logsModel.Logs = _logsModel.InformationLogs.Concat(_logsModel.Errors.Cast<Log>().Concat(_logsModel.WarningLogs.Cast<Log>())).ToList();
            OrderByDesc();
        }

        public async Task GetLogs()
        {
            _logsModel.Errors = new List<Error>();
            _logsModel.InformationLogs = new List<Information>();
            _logsModel.WarningLogs = new List<Warning>();
            _logsModel.Logs = (await GetAllLogsRequestHandler.HandleRequest(new GetAllLogsRequest{ ClientId = ClientId, LowerBoundDateTime = DateTime.Now.AddDays(-1), UpperBoundDateTime = DateTime.Now })).ToList();
        }
        public void OrderByDesc()
        {
            _logsModel.Errors = _logsModel.Errors.OrderByDescending(x => x.DateTimeOfOccurrence).ToList();
            _logsModel.InformationLogs = _logsModel.InformationLogs.OrderByDescending(x => x.DateTimeOfOccurrence).ToList();
            _logsModel.WarningLogs = _logsModel.WarningLogs.OrderByDescending(x => x.DateTimeOfOccurrence).ToList();
            _logsModel.Logs = _logsModel.Logs.OrderByDescending(x => x.DateTimeOfOccurrence).ToList();
        }

        //public void OrderByAsc()
        //{
        //    _logsModel.Errors = _logsModel.Errors.OrderBy(x => x.DateTimeOfOccurrence).ToList();
        //    _logsModel.InformationLogs = _logsModel.InformationLogs.OrderBy(x => x.DateTimeOfOccurrence).ToList();
        //    _logsModel.Logs = _logsModel.Logs.OrderBy(x => x.DateTimeOfOccurrence).ToList();
        //}

        void OnUpperDateChanged(DateTime? date)
        {
            _logsModel.UpperBoundDateTime = date ?? DateTime.MinValue;
        }
        void OnLowerDateChanged(DateTime? date)
        {
            _logsModel.LowerBoundDateTime = date ?? DateTime.MinValue;
        }
    }
}
