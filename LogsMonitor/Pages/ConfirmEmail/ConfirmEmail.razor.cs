﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Features;
using LogsMonitor.Features.EmailConfirmationFeature;
using LogsMonitor.Features.UserFeature;
using Microsoft.AspNetCore.Components;

namespace LogsMonitor.Pages.ConfirmEmail
{
    public partial class ConfirmEmail : ComponentBase
    {
        [Parameter]
        public string UserId { get; set; }
        [Parameter]
        public string Token { get; set; }
        [Inject]
        protected NavigationManager NavigationManager { get; set; }

        [Inject] protected IRequestHandler<ConfirmEmailRequest, bool> ConfirmEmailRequestHandler { get; set; }


        protected override async Task OnInitializedAsync()
        {
            await ConfirmEmailRequestHandler.HandleRequest(new ConfirmEmailRequest {UserId = UserId, Token = Token});
            await base.OnInitializedAsync();
        }

        private void NavigateToLogin()
        {
            NavigationManager.NavigateTo("Login");
        }
    }
}
