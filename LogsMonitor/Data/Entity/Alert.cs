﻿using System;

namespace LogsMonitor.Data.Entity
{
    public class Alert
    {
        public string Id { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
        public DateTime LastTriggered { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public bool Deleted { get; set; }
    }
}
