﻿using System;

namespace LogsMonitor.Data.Entity
{
    public class Information : Log
    {
        public Information(string clientId, string summary, DateTime dateTimeOfOccurrence) : base(clientId, summary, dateTimeOfOccurrence) { }
        public string InfoText { get; set; }
    }
}
