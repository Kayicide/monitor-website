﻿using System;

namespace LogsMonitor.Data.Entity
{
    public class Log
    {
        public Log(string clientId, string summary, DateTime dateTimeOfOccurrence)
        {
            ClientId = clientId;
            Summary = summary;
            DateTimeOfOccurrence = dateTimeOfOccurrence;
        }

        public string LogId { get; set; }
        public string ClientId { get; init; }
        public string Identifier { get; init; }
        public string Summary { get; init; }
        public string ObjectDump { get; set; }
        public DateTime DateTimeOfOccurrence { get; init; }

    }
}
