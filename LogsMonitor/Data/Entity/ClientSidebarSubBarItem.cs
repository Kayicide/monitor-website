﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Data.Entity
{
    public class ClientSidebarSubBarItem
    {
        public Client Client { get; set; }
        public ClientSidebarSubBarItem SubBarItem { get; set; }
    }
}
