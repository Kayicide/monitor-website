﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Features.LogFeature.HttpRequests
{
    public class HttpGetAllInfoLogsRequest
    {
        public string ClientId { get; set; }
        public DateTime LowerBoundDateTime { get; set; }
        public DateTime UpperBoundDateTime { get; set; }
        public string Identifier { get; init; }
        public bool IdentifierExact { get; init; }
        public string Summary { get; init; }
        public bool SummaryExact { get; init; }
        public string InfoText { get; set; }
        public bool InfoTextExact { get; set; }
    }
}
