﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Features.LogFeature.HttpRequests
{
    public class HttpGetAllErrorLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public DateTime LowerBoundDateTime { get; set; }
        public DateTime UpperBoundDateTime { get; set; }
        public string Identifier { get; init; }
        public bool IdentifierExact { get; init; }
        public string Summary { get; init; }
        public bool SummaryExact { get; init; }
        public int Severity { get; init; }
        public string StackTrace { get; init; }
        public bool StackTraceExact { get; init; }
    }
}
