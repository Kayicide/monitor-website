﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Features.LogFeature.HttpRequests
{
    public class GetAllLogsHttpRequest
    {
        public string ClientId { get; set; }
        public DateTime LowerBoundDateTime { get; set; }
        public DateTime UpperBoundDateTime { get; set; }
        public string Summary { get; init; }
        public bool SummaryExact { get; init; }
        public string Identifier { get; init; }
        public bool IdentifierExact { get; set; }
    }
}
