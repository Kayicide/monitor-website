﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.LogFeature.HttpRequests;

namespace LogsMonitor.Features.LogFeature
{
    public class GetAllInformationLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public string InfoText { get; set; } = string.Empty;
        public bool InfoTextExact { get; set; } = false;
    }
    public class GetAllInformationLogsRequestHandler : IRequestHandler<GetAllInformationLogsRequest, IList<Information>>
    {
        private readonly HttpClient _httpClient;

        public GetAllInformationLogsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }
        public async Task<IList<Information>> HandleRequest(GetAllInformationLogsRequest request)
        {
            var body = new HttpGetAllInfoLogsRequest
            {
                ClientId = request.ClientId,
                Identifier = request.Identifier, 
                IdentifierExact = request.IdentifierExact,
                InfoText = request.InfoText,
                InfoTextExact = request.InfoTextExact, 
                LowerBoundDateTime = request.LowerBoundDateTime,
                UpperBoundDateTime = request.UpperBoundDateTime,
                Summary = request.Summary, 
                SummaryExact = request.SummaryExact
            };

            var response = await _httpClient.PostAsJsonAsync("Information/GetAll", body);
            var result = await response.Content.ReadFromJsonAsync<IList<Information>>();

            return result;
        }
    }
}
