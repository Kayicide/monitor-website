﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.LogFeature.HttpRequests;

namespace LogsMonitor.Features.LogFeature
{
    public class GetAllWarningLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; init; } = false;
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public string WarningText { get; set; } = string.Empty;
        public bool WarningTextExact { get; set; } = false;
    }
    public class GetAllWarningLogsRequestHandler : IRequestHandler<GetAllWarningLogsRequest, IList<Warning>>
    {
        private readonly HttpClient _httpClient;
        public GetAllWarningLogsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }
        public async Task<IList<Warning>> HandleRequest(GetAllWarningLogsRequest request)
        {
            var body = new HttpGetAllWarningLogsRequest
            {
                ClientId = request.ClientId, 
                UpperBoundDateTime = request.UpperBoundDateTime,
                LowerBoundDateTime = request.LowerBoundDateTime,
                Identifier = request.Identifier, 
                IdentifierExact = request.IdentifierExact,
                Summary = request.Summary,
                SummaryExact = request.SummaryExact,
                WarningText = request.WarningText,
                WarningTextExact = request.WarningTextExact
            };

            var response = await _httpClient.PostAsJsonAsync("Warning/GetAll", body);
            var result = await response.Content.ReadFromJsonAsync<IList<Warning>>();

            return result;
        }
    }
}
