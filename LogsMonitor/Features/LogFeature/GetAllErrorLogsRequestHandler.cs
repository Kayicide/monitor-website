﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.LogFeature.HttpRequests;

namespace LogsMonitor.Features.LogFeature
{
    public class GetAllErrorLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; init; } = false;
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public int Severity { get; init; } = 0;
        public string StackTrace { get; init; } = string.Empty;
        public bool StackTraceExact { get; init; } = false;
    }
    public class GetAllErrorLogsRequestHandler : IRequestHandler<GetAllErrorLogsRequest, IList<Error>>
    {
        private readonly HttpClient _httpClient;

        public GetAllErrorLogsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }
        public async Task<IList<Error>> HandleRequest(GetAllErrorLogsRequest request)
        {
            var body = new HttpGetAllErrorLogsRequest
            {
                ClientId = request.ClientId,
                Identifier = request.Identifier, 
                IdentifierExact = request.IdentifierExact,
                Summary = request.Summary, 
                StackTrace = request.StackTrace, 
                LowerBoundDateTime = request.LowerBoundDateTime,
                UpperBoundDateTime = request.UpperBoundDateTime,
                Severity = request.Severity,
                StackTraceExact = request.StackTraceExact,
                SummaryExact = request.SummaryExact
            };

            var response = await _httpClient.PostAsJsonAsync("Error/GetAll", body);
            var result = await response.Content.ReadFromJsonAsync<IList<Error>>();

            return result;
        }
    }
}
