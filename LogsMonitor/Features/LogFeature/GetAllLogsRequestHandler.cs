﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.LogFeature.HttpRequests;
using LogsMonitor.Utility;
using Newtonsoft.Json;

namespace LogsMonitor.Features.LogFeature
{
    public class GetAllLogsRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public DateTime LowerBoundDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperBoundDateTime { get; set; } = DateTime.MinValue;
        public string Summary { get; init; } = string.Empty;
        public bool SummaryExact { get; init; } = false;
        public string Identifier { get; init; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
    }
    public class GetAllLogsRequestHandler : IRequestHandler<GetAllLogsRequest, IList<Log>>
    {
        private readonly HttpClient _httpClient;

        public GetAllLogsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }

        public async Task<IList<Log>> HandleRequest(GetAllLogsRequest request)
        {
            var body = new GetAllLogsHttpRequest
            {
                ClientId = request.ClientId,
                Identifier = request.Identifier,
                IdentifierExact = request.IdentifierExact,
                Summary = request.Summary,
                SummaryExact = request.SummaryExact,
                LowerBoundDateTime = request.LowerBoundDateTime,
                UpperBoundDateTime = request.UpperBoundDateTime
            };

            var response = await _httpClient.PostAsJsonAsync("Log", body);
            var result = response.Content.ReadAsStringAsync().Result;

            var logs = JsonConvert.DeserializeObject<IList<Log>>(result, new JsonSerializerSettings{TypeNameHandling = TypeNameHandling.Objects, SerializationBinder = new KnownTypesBinder()});

            return logs;
        }
    }
}
