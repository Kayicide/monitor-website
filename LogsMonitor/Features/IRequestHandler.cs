﻿using System.Threading.Tasks;

namespace LogsMonitor.Features
{
    public interface IRequestHandler<in TRequest, TReturn>
    {
        public Task<TReturn> HandleRequest(TRequest request);
    }
}
