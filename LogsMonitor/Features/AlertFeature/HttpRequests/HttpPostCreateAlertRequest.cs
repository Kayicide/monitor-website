﻿namespace LogsMonitor.Features.AlertFeature.HttpRequests
{
    public class HttpPostCreateAlertRequest
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
    }
}
