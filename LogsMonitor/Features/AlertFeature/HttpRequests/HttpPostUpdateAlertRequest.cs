﻿namespace LogsMonitor.Features.AlertFeature.HttpRequests
{
    public class HttpPostUpdateAlertRequest
    {
        public string AlertId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
        public bool? Deleted { get; set; }
    }
}
