﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.AlertFeature.HttpRequests;

namespace LogsMonitor.Features.AlertFeature
{
    public class UpdateAlertRequest : IRequest
    {
        public string AlertId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; }
        public string LogIdentifier { get; set; }
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
        public bool? Deleted { get; set; }
    }

    public class UpdateAlertRequestHandler : IRequestHandler<UpdateAlertRequest, Alert>
    {
        private readonly HttpClient _httpClient;
        public UpdateAlertRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }
        public async Task<Alert> HandleRequest(UpdateAlertRequest request)
        {
            var body = new HttpPostUpdateAlertRequest
            {
                AlertId = request.AlertId,
                Name = request.Name,
                LogType = request.LogType,
                LogIdentifier = request.LogIdentifier,
                ThresholdAmount = request.ThresholdAmount,
                TimeSpan = request.TimeSpan,
                Deleted = request.Deleted
            };
            var response = await _httpClient.PostAsJsonAsync("Alert/Update", body);
            return await response.Content.ReadFromJsonAsync<Alert>();
        }
    }
}
