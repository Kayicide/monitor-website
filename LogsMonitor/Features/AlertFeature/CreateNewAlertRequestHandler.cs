﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.AlertFeature.HttpRequests;

namespace LogsMonitor.Features.AlertFeature
{
    public class CreateNewAlertRequest : IRequest
    {
        public string ClientId { get; set; }
        public string Name { get; set; }
        public string LogType { get; set; } = "";
        public string LogIdentifier { get; set; } = "";
        public int TimeSpan { get; set; }
        public int ThresholdAmount { get; set; }
    }
    public class CreateNewAlertRequestHandler : IRequestHandler<CreateNewAlertRequest, Alert>
    {
        private readonly HttpClient _httpClient;
        public CreateNewAlertRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }
        public async Task<Alert> HandleRequest(CreateNewAlertRequest request)
        {
            var body = new HttpPostCreateAlertRequest
            {
                ClientId = request.ClientId,
                Name = request.Name,
                LogIdentifier = request.LogIdentifier,
                LogType = request.LogType,
                ThresholdAmount = request.ThresholdAmount,
                TimeSpan = request.TimeSpan
            };
            var response = await _httpClient.PostAsJsonAsync("Alert/Create", body);
            return await response.Content.ReadFromJsonAsync<Alert>();
        }
    }
}
