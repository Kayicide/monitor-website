﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.AlertFeature
{
    public class GetAllAlertsRequest : IRequest
    {
        public string ClientId { get; set; }
    }
    public class GetAllAlertsRequestHandler : IRequestHandler<GetAllAlertsRequest, IList<Alert>>
    {
        private readonly HttpClient _httpClient;
        public GetAllAlertsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("LoggingService");
        }
        public async Task<IList<Alert>> HandleRequest(GetAllAlertsRequest request)
        {
            var response = await _httpClient.GetFromJsonAsync<IList<Alert>>($"Alert?clientId={request.ClientId}");
            return response;
        }
    }
}
