﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.ClientFeature.HttpRequests;
using LogsMonitor.Utility;
using Microsoft.IdentityModel.Tokens;

namespace LogsMonitor.Features.Shared
{
    public class GenerateJwtTokenRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
    }
    public class GenerateJwtTokenRequestHandler : IRequestHandler<GenerateJwtTokenRequest, Client>
    {
        private readonly HttpClient _httpClient;
        private readonly Secrets _secrets;
        public GenerateJwtTokenRequestHandler(Secrets secrets, IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
            _secrets = secrets;
        }
        public async Task<Client> HandleRequest(GenerateJwtTokenRequest request)
        {
            //TODO: check that the tokens created by this actuually work lol
            var body = new UpdateJwtHttpRequest { ClientId = request.ClientId, CurrentUser = request.CurrentUser, Jwt = GenToken( _secrets.LoggingServiceSecret, request.ClientId) };
            var response = await _httpClient.PostAsJsonAsync("Client/UpdateJwt", body);
            if (!response.IsSuccessStatusCode)
                return null;
            var result = await response.Content.ReadFromJsonAsync<Client>();
            return result;
        }

        private string GenToken(string secret, string clientId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, clientId),
                    new Claim(ClaimTypes.Role, "Client")
                }),
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
