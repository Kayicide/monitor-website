﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Services.HttpRequests;

namespace LogsMonitor.Features.UserFeature
{
    public class GetAllUsersRequest : IRequest
    {
        public string ClientId { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
        public bool EmailExact { get; set; } = false;
        public string FirstName { get; set; } = string.Empty;
        public bool FirstNameExact { get; set; } = false;
        public string LastName { get; set; } = string.Empty;
        public bool LastNameExact { get; set; } = false;
        public bool Deleted { get; set; } = false;
    }
    public class GetAllUsersRequestHandler : IRequestHandler<GetAllUsersRequest, IList<User>>
    {
        private readonly HttpClient _httpClient;
        public GetAllUsersRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<IList<User>> HandleRequest(GetAllUsersRequest request)
        {
            var body = new GetAllUsersHttpRequest
            {
                ClientId = request.ClientId,
                Email = request.Email,
                EmailExact = request.EmailExact,
                FirstName = request.FirstName,
                FirstNameExact = request.FirstNameExact,
                LastName = request.LastName,
                LastNameExact = request.LastNameExact,
                IncludeDeleted = request.Deleted
            };
            var response = await _httpClient.PostAsJsonAsync("User/Users", body);
            return await response.Content.ReadFromJsonAsync<IList<User>>();
        }
    }
}
