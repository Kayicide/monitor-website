﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.UserFeature
{
    public class GetUserRequest
    {
        public string UserId { get; set; }
    }
    public class GetUserRequestHandler : IRequestHandler<GetUserRequest, User>
    {
        private readonly HttpClient _httpClient;
        public GetUserRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<User> HandleRequest(GetUserRequest request)
        {
            var result = await _httpClient.GetFromJsonAsync<User>($"User?userId={request.UserId}");
            return result;
        }
    }
}
