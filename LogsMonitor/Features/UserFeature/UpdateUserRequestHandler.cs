﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.UserFeature.HttpRequests;
using LogsMonitor.Utility;

namespace LogsMonitor.Features.UserFeature
{
    public class UpdateUserRequest
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool? Deleted { get; set; }
        public string Role { get; set; }
    }
    public class UpdateUserRequestHandler : IRequestHandler<UpdateUserRequest, User>
    {
        private readonly HttpClient _httpClient;
        public UpdateUserRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<User> HandleRequest(UpdateUserRequest request)
        {
            SaltedHash.HashSalt saltedHash = null;
            if (!string.IsNullOrEmpty(request.Password))
                saltedHash = SaltedHash.GenerateSaltedHash(request.Password);

            var body = new UpdateUserHttpRequest
            {
                UserId = request.UserId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Hash = saltedHash != null ? saltedHash.Hash : string.Empty,
                Salt = saltedHash != null ? saltedHash.Salt : string.Empty,
                Deleted = request.Deleted, 
                Role = request.Role, 
                Email = request.Email
            };
            var response = await _httpClient.PutAsJsonAsync("User", body);
            var result = await response.Content.ReadFromJsonAsync<User>();
            return result;
        }
    }
}
