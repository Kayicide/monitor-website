﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Features;

namespace LogsMonitor.Features.UserFeature
{
    public class JoinClientRequest
    {
        public string CurrentUser { get; set; }
        public string ClientJoinToken { get; set; }
    }
    public class JoinClientRequestHandler : IRequestHandler<JoinClientRequest, bool>
    {
        private readonly HttpClient _httpClient;
        public JoinClientRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }

        public async Task<bool> HandleRequest(JoinClientRequest request)
        {
            var content = new StringContent("");
            var result = await _httpClient.PatchAsync($"user/JoinTeam?userId={request.CurrentUser}&clientJoinToken={request.ClientJoinToken}", content);
            return result.IsSuccessStatusCode;
        }
    }
}
