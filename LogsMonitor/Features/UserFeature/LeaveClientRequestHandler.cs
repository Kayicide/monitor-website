﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using LoggingService.Features;

namespace LogsMonitor.Features.UserFeature
{
    public class LeaveClientRequest
    {
        public string CurrentUser { get; set; }
        public string UserId { get; set; }
        public string ClientId { get; set; }
    }
    public class LeaveClientRequestHandler : IRequestHandler<LeaveClientRequest, bool>
    {
        private readonly HttpClient _httpClient;
        public LeaveClientRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<bool> HandleRequest(LeaveClientRequest request)
        {
            var content = new StringContent("");
            var result = await _httpClient.PatchAsync($"user/LeaveTeam?currentUser={request.CurrentUser}&userId={request.UserId}&clientId={request.ClientId}", content);
            return result.IsSuccessStatusCode;
        }
    }
}
