﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.UserFeature.HttpRequests;
using LogsMonitor.Services.HttpRequests;
using LogsMonitor.Utility;

namespace LogsMonitor.Features.UserFeature
{
    public class RegisterUserRequest : IRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
    }
    public class RegisterUserRequestHandler : IRequestHandler<RegisterUserRequest, User>
    {
        private readonly HttpClient _httpClient;
        public RegisterUserRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<User> HandleRequest(RegisterUserRequest request)
        {
            var saltedHash = SaltedHash.GenerateSaltedHash(request.Password);
            var registerUserHttpRequest = new RegisterUserHttpRequest
            {
                Email = request.Email, 
                FirstName = request.FirstName,
                LastName = request.LastName,
                Salt = saltedHash.Salt,
                Hash = saltedHash.Hash
            };
            var response = (await _httpClient.PostAsJsonAsync("user/register", registerUserHttpRequest));
            var result = await response.Content.ReadFromJsonAsync<User>();

            return result;
        }
    }
}
