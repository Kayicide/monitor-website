﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Features.UserFeature.HttpRequests
{
    public class UpdateUserHttpRequest
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool? Deleted { get; set; }
        public string Role { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }
    }
}
