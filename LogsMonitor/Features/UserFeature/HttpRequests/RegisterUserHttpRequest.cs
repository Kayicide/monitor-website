﻿namespace LogsMonitor.Features.UserFeature.HttpRequests
{
    public class RegisterUserHttpRequest
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Hash { get; set; }
        public string Salt { get; set; }
    }
}
