﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.DashboardFeature
{
    public class GetDashboardRequest
    {
        public string DashboardId { get; set; }
    }
    public class GetDashboardRequestHandler : IRequestHandler<GetDashboardRequest, Dashboard>
    {
        private readonly HttpClient _httpClient;
        public GetDashboardRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("DashService");
        }
        public async Task<Dashboard> HandleRequest(GetDashboardRequest request)
        {
            return await _httpClient.GetFromJsonAsync<Dashboard>($"Dashboard/Dashboard?dashboardId={request.DashboardId}");
        }
    }
}
