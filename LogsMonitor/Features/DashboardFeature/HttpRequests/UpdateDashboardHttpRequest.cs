﻿using System.ComponentModel.DataAnnotations;

namespace LogsMonitor.Features.DashboardFeature.HttpRequests
{
    public class UpdateDashboardHttpRequest
    {
        public string DashboardId { get; set; }
        public string Name { get; set; }
        public bool ShowTotalLogs { get; set; }
        public bool ShowErrors { get; set; }
        public bool ShowWarnings { get; set; }
        public bool ShowInformation { get; set; }
        public int TimeScale { get; set; }
        public string LogIdentifier { get; set; }
        public bool LogIdentifierExact { get; set; }
        public bool Popup { get; set; }
        public int NumberToTrigger { get; set; }
    }
}
