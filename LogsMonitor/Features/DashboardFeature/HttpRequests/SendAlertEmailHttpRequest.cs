﻿namespace LogsMonitor.Features.DashboardFeature.HttpRequests
{
    public class SendAlertEmailHttpRequest
    {
        public string DashboardId { get; set; }
        public string ClientId { get; set; }
        public int ActuallyNumber { get; set; }
    }
}
