﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Features.DashboardFeature.HttpRequests;

namespace LogsMonitor.Features.DashboardFeature
{
    public class SendAlertEmailRequest : IRequest
    {
        public string ClientId { get; set; }
        public string DashboardId { get; set; }
        public int ActualNumber { get; set; }
    }
    public class SendAlertEmailRequestHandler : IRequestHandler<SendAlertEmailRequest, bool>
    {
        private readonly HttpClient _httpClient;
        public SendAlertEmailRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("DashService");
        }

        public async Task<bool> HandleRequest(SendAlertEmailRequest request)
        {
            var body = new SendAlertEmailHttpRequest
            {
                ClientId = request.ClientId,
                DashboardId = request.DashboardId,
                ActuallyNumber = request.ActualNumber
            };
            var response = await _httpClient.PostAsJsonAsync("Alert", body);
            return response.IsSuccessStatusCode;
        }
    }
}
