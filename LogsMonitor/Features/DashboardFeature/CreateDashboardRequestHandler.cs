﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.DashboardFeature
{
    public class CreateDashboardRequest
    {
        public string Name { get; set; }
        public string ClientId { get; set; }
    }
    public class CreateDashboardRequestHandler : IRequestHandler<CreateDashboardRequest, Dashboard>
    {
        private readonly HttpClient _httpClient;
        public CreateDashboardRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("DashService");
        }

        public async Task<Dashboard> HandleRequest(CreateDashboardRequest request)
        {
            string url = $"Dashboard/Create?dashboardName={request.Name}&clientId={request.ClientId}";
            var response = await _httpClient.PostAsJsonAsync(url, string.Empty);
            var result = await response.Content.ReadFromJsonAsync<Dashboard>();

            return result;
        }
    }
}
