﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.DashboardFeature
{
    public class GetAllDashboardsRequest
    {
        public string ClientId { get; set; } = string.Empty;
    }
    public class GetAllDashboardsRequestHandler : IRequestHandler<GetAllDashboardsRequest, IList<Dashboard>>
    {
        private readonly HttpClient _httpClient;
        public GetAllDashboardsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("DashService");
        }

        public async Task<IList<Dashboard>> HandleRequest(GetAllDashboardsRequest request)
        {
            string path = !string.IsNullOrEmpty(request.ClientId) ? $"Dashboard/Dashboards?clientId={request.ClientId}" : "Dashboard/Dashboards";
            return await _httpClient.GetFromJsonAsync<IList<Dashboard>>(path);
        }
    }
}
