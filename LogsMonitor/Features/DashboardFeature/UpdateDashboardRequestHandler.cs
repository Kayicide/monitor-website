﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.DashboardFeature.HttpRequests;

namespace LogsMonitor.Features.DashboardFeature
{
    public class UpdateDashboardRequest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool ShowTotalLogs { get; set; }
        public bool ShowErrors { get; set; }
        public bool ShowWarnings { get; set; }
        public bool ShowInformation { get; set; }
        public XAxisTimeScale TimeScale { get; set; }
        public string LogIdentifier { get; set; }
        public bool LogIdentifierExact { get; set; }
        public bool Popup { get; set; }
        public int NumberToTrigger { get; set; }
    }
    public class UpdateDashboardRequestHandler : IRequestHandler<UpdateDashboardRequest, Dashboard>
    {
        private readonly HttpClient _httpClient;
        public UpdateDashboardRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("DashService");
        }
        public async Task<Dashboard> HandleRequest(UpdateDashboardRequest request)
        {
            var body = new UpdateDashboardHttpRequest
            {
                DashboardId = request.Id,
                TimeScale = (int) request.TimeScale,
                ShowErrors = request.ShowErrors,
                ShowWarnings = request.ShowWarnings,
                ShowInformation = request.ShowInformation,
                ShowTotalLogs = request.ShowTotalLogs,
                Name = request.Name,
                LogIdentifierExact = request.LogIdentifierExact,
                LogIdentifier = request.LogIdentifier,
                Popup = request.Popup,
                NumberToTrigger = request.NumberToTrigger
            };
            var response = await _httpClient.PostAsJsonAsync("Dashboard/Update", body);
            return await response.Content.ReadFromJsonAsync<Dashboard>();
        }
    }
}
