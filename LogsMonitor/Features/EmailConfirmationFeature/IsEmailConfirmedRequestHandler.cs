﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;

namespace LogsMonitor.Features.EmailConfirmationFeature
{
    public class IsEmailConfirmedRequest : IRequest
    {
        public string UserId { get; set; }
    }
    public class IsEmailConfirmedRequestHandler : IRequestHandler<IsEmailConfirmedRequest, bool>
    {
        private readonly HttpClient _httpClient;
        public IsEmailConfirmedRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }

        public async Task<bool> HandleRequest(IsEmailConfirmedRequest request)
        {
            return await _httpClient.GetFromJsonAsync<bool>($"EmailConfirmation?userId={request.UserId}");
        }
    }
}
