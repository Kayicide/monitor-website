﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using LoggingService.Features;

namespace LogsMonitor.Features.EmailConfirmationFeature
{
    public class ConfirmEmailRequest : IRequest
    {
        public string UserId { get; set; }
        public string Token { get; set; }
    }
    public class ConfirmEmailRequestHandler : IRequestHandler<ConfirmEmailRequest, bool>
    {
        private readonly HttpClient _httpClient;
        public ConfirmEmailRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<bool> HandleRequest(ConfirmEmailRequest request)
        {
            var response = await _httpClient.PostAsync($"EmailConfirmation?userId={request.UserId}&token={request.Token}", new StringContent("", Encoding.UTF8, "application/json"));
            return await response.Content.ReadFromJsonAsync<bool>();
        }
    }
}
