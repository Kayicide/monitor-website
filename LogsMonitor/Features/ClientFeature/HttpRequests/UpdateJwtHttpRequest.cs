﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Features.ClientFeature.HttpRequests
{
    public class UpdateJwtHttpRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string Jwt { get; set; }
    }
}
