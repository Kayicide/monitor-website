﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Features.ClientFeature.HttpRequests
{
    public class UpdateClientHttpRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; } = "";
        public bool? Active { get; set; } = null;
    }
}
