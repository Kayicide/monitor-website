﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.ClientFeature.HttpRequests
{
    public class CreateClientHttpRequest
    {
        public string Name { get; set; }
        public ClientType Type { get; set; }
        public string UserId { get; set; }
    }
}
