﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.ClientFeature
{
    public class AddAdminRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string UserId { get; set; }
    }
    public class AddAdminRequestHandler : IRequestHandler<AddAdminRequest, Client>
    {
        private readonly HttpClient _httpClient;
        public AddAdminRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<Client> HandleRequest(AddAdminRequest request)
        {
            var content = new StringContent("");
            var response = await _httpClient.PatchAsync($"Client/AddAdmin?currentUser={request.CurrentUser}&clientId={request.ClientId}&userId={request.UserId}", content);
            var result = await response.Content.ReadFromJsonAsync<Client>();
            return result;
        }
    }
}
