﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.ClientFeature
{
    public class RegenJoinTokenRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
    }
    public class RegenJoinTokenRequestHandler : IRequestHandler<RegenJoinTokenRequest, Client>
    {
        private readonly HttpClient _httpClient;
        public RegenJoinTokenRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<Client> HandleRequest(RegenJoinTokenRequest request)
        {
            var content = new StringContent("");
            var response = await _httpClient.PatchAsync($"Client/RegenJoinToken?currentUser={request.CurrentUser}&clientId={request.ClientId}", content);
            var result = await response.Content.ReadFromJsonAsync<Client>();
            return result;
        }
    }
}
