﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.ClientFeature
{
    public class RemoveAdminRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string UserId { get; set; }
    }
    public class RemoveAdminRequestHandler : IRequestHandler<RemoveAdminRequest, Client>
    {
        private readonly HttpClient _httpClient;
        public RemoveAdminRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<Client> HandleRequest(RemoveAdminRequest request)
        {
            var content = new StringContent("");
            var response = await _httpClient.PatchAsync($"Client/RemoveAdmin?currentUser={request.CurrentUser}&clientId={request.ClientId}&userId={request.UserId}", content);
            var result = await response.Content.ReadFromJsonAsync<Client>();
            return result;
        }
    }
}
