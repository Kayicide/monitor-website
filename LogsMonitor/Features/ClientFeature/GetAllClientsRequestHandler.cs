﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.ClientFeature.HttpRequests;

namespace LogsMonitor.Features.ClientFeature
{
    public class GetAllClientsRequest
    {
        public string UserId { get; set; } = string.Empty;
        public string Name { get; set; } = string.Empty;
        public bool NameExact { get; set; } = false;
        public string JoinToken { get; set; } = string.Empty;
        public bool? Active { get; set; } = null;
        public DateTime LowerCreatedDateTime { get; set; } = DateTime.MinValue;
        public DateTime UpperCreatedDateTime { get; set; } = DateTime.MinValue;
    }
    public class GetAllClientsRequestHandler : IRequestHandler<GetAllClientsRequest, IList<Client>>
    {
        private readonly HttpClient _httpClient;
        public GetAllClientsRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<IList<Client>> HandleRequest(GetAllClientsRequest request)
        {
            var body = new GetAllClientsHttpRequest
            {
                UserId = request.UserId,
                Name = request.Name, 
                NameExact = request.NameExact,
                JoinToken = request.JoinToken,
                Active = request.Active,
                LowerCreatedDateTime = request.LowerCreatedDateTime,
                UpperCreatedDateTime = request.UpperCreatedDateTime
            };

            var response = await _httpClient.PostAsJsonAsync("client/clients", body);
            var result = await response.Content.ReadFromJsonAsync<IList<Client>>();

            return result;
        }
    }
}
