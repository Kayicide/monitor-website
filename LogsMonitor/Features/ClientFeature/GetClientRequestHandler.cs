﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Features.ClientFeature
{
    public class GetClientRequest
    {
        public GetClientRequest(string clientId)
        {
            ClientId = clientId;
        }
        public string ClientId { get;}
    }
    public class GetClientRequestHandler : IRequestHandler<GetClientRequest, Client>
    {
        private readonly HttpClient _httpClient;
        public GetClientRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }

        public async Task<Client> HandleRequest(GetClientRequest request)
        {
            var result = new Client();
            try
            {
                result = await _httpClient.GetFromJsonAsync<Client>($"Client?clientId={request.ClientId}");
            }
            catch (Exception ex)
            {

            }
            return result;
        }
    }
}
