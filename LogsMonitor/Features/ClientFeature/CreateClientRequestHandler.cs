﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.ClientFeature.HttpRequests;
using LogsMonitor.Features.UserFeature.HttpRequests;

namespace LogsMonitor.Features.ClientFeature
{
    public class CreateClientRequest
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public ClientType Type { get; set; }
    }
    public class CreateClientRequestHandler : IRequestHandler<CreateClientRequest, Client>
    {
        private readonly HttpClient _httpClient;
        public CreateClientRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<Client> HandleRequest(CreateClientRequest request)
        {
            var body = new CreateClientHttpRequest {UserId = request.UserId, Type = request.Type, Name = request.Name};
            var response = await _httpClient.PostAsJsonAsync("client/create", body);
            var result = await response.Content.ReadFromJsonAsync<Client>();

            return result;
        }
    }
}
