﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.ClientFeature.HttpRequests;

namespace LogsMonitor.Features.ClientFeature
{
    public class UpdateClientNameRequest
    {
        public string CurrentUser { get; set; }
        public string ClientId { get; set; }
        public string Name { get; set; } = "";
        public bool? Active { get; set; } = null;
    }
    public class UpdateClientNameRequestHandler : IRequestHandler<UpdateClientNameRequest, Client>
    {
        private readonly HttpClient _httpClient;
        public UpdateClientNameRequestHandler(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
        }
        public async Task<Client> HandleRequest(UpdateClientNameRequest request)
        {
            var body = new UpdateClientHttpRequest
            {
                CurrentUser = request.CurrentUser,
                ClientId = request.ClientId,
                Name = request.Name,
                Active = request.Active
            };

            var response = await _httpClient.PostAsJsonAsync("client/update", body);
            if (!response.IsSuccessStatusCode)
                return null;

            return await response.Content.ReadFromJsonAsync<Client>();
        }
    }
}
