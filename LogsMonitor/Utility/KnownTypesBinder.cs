﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;
using Newtonsoft.Json.Serialization;

namespace LogsMonitor.Utility
{
    public class KnownTypesBinder : ISerializationBinder
    {
        public IList<Type> KnownTypes = new List<Type> {typeof(Error), typeof(Information), typeof(Warning), typeof(Log)};

        public Type BindToType(string assemblyName, string typeName)
        {
            foreach (var knownType in KnownTypes)
            {
                var name = knownType.Name;
                if (typeName.Contains(name))
                {
                    return knownType;
                }
            }

            return null;
        }

        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            assemblyName = null;
            typeName = serializedType.Name;
        }
    }
}
