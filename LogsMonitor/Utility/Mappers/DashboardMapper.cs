﻿using LogsMonitor.Data.Entity;
using LogsMonitor.Models.Dashboard;

namespace LogsMonitor.Utility.Mappers
{
    public class DashboardMapper : IMapper<Dashboard, DashboardModel>
    {
        public DashboardModel Map(Dashboard toMap)
        {
            return new()
            {
                Id = toMap.Id,
                ShowTotalLogs = toMap.ShowTotalLogs,
                TimeScale = toMap.TimeScale,
                ShowErrors = toMap.ShowErrors,
                Name = toMap.Name,
                ShowInformation = toMap.ShowInformation,
                ShowWarnings = toMap.ShowWarnings,
                LogIdentifier = toMap.LogIdentifier,
                LogIdentifierExact = toMap.LogIdentifierExact,
                Popup = toMap.Popup,
                NumberToTrigger = toMap.NumberToTrigger
            };
        }
    }
}
