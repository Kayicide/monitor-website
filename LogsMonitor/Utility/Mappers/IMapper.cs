﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Utility.Mappers
{
    public interface IMapper<in TIn, out TReturn>
    {
        public TReturn Map(TIn toMap);
    }
}
