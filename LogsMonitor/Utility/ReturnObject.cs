﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Utility
{
    public class ReturnObject
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public object Object { get; set; }

    }
}
