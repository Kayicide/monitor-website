﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Utility
{
    public class Secrets
    {
        public string AuthServiceSecret { get; set; }
        public string LoggingServiceSecret { get; set; }
    }
}
