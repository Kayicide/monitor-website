﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using Blazored.SessionStorage;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.EmailConfirmationFeature;
using LogsMonitor.Features.UserFeature;
using LogsMonitor.Services.HttpRequests;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Utility
{
    public class CustomAuthenticationStateProvider : AuthenticationStateProvider
    {
        private readonly HttpClient _httpClient;
        private readonly ISessionStorageService _sessionStorage;
        private readonly ILocalStorageService _localStorage;
        private readonly IRequestHandler<IsEmailConfirmedRequest, bool> _isEmailConfirmedRequestHandler;
        private readonly IRequestHandler<GetUserRequest, User> _getUserRequestHandler;
        public CustomAuthenticationStateProvider(IHttpClientFactory httpClientFactory, ISessionStorageService sessionStorage, ILocalStorageService localStorage, IRequestHandler<IsEmailConfirmedRequest, bool> isEmailConfirmedRequestHandler, IRequestHandler<GetUserRequest, User> getUserRequestHandler)
        {
            _httpClient = httpClientFactory.CreateClient("AuthService");
            _sessionStorage = sessionStorage;
            _localStorage = localStorage;
            _isEmailConfirmedRequestHandler = isEmailConfirmedRequestHandler;
            _getUserRequestHandler = getUserRequestHandler;
        }

        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var user = await _sessionStorage.GetItemAsync<User>("User") ?? await _localStorage.GetItemAsync<User>("User");

            if (user != null)
            {
                user = await _getUserRequestHandler.HandleRequest(new GetUserRequest { UserId = user.Id });
                if (user.Deleted)
                    user = null;
            }

            return GenAuthState(user);
        }

        public async Task<ReturnObject> UpdateAuthState(string email, string password, bool stayLoggedIn)
        {
            try
            {
                var response = (await _httpClient.PostAsJsonAsync("User/Users", new GetAllUsersHttpRequest { Email = email }));
                var user = (await response.Content.ReadFromJsonAsync<IList<User>>())?.FirstOrDefault();

                if (user is null)
                    return new ReturnObject{Success = false, ErrorMessage = "Email or Password is incorrect!"};

                var saltBytes = Convert.FromBase64String(user.HashSalt.Salt);
                var encrypted = new Rfc2898DeriveBytes(password, saltBytes, 10000);
                if (Convert.ToBase64String(encrypted.GetBytes(256)) == user.HashSalt.Hash)
                {
                    if (!(await _isEmailConfirmedRequestHandler.HandleRequest(new IsEmailConfirmedRequest { UserId = user.Id })))
                        return new ReturnObject { Success = false, ErrorMessage = "Email has not been confirmed!" };

                    if (!stayLoggedIn)
                        await _sessionStorage.SetItemAsync("User", user);
                    else
                        await _localStorage.SetItemAsync("User", user);

                    NotifyAuthenticationStateChanged(Task.FromResult(GenAuthState(user)));
                    return new ReturnObject {Success = true};
                }
                return new ReturnObject { Success = false, ErrorMessage = "Email or Password is incorrect!" };
            }
            catch (Exception ex)
            {
                return new ReturnObject{Success = false, ErrorMessage = "An Error has Occurred"};
            }
        }

        public void Logout()
        {
            _sessionStorage.RemoveItemAsync("User");
            _localStorage.RemoveItemAsync("User");

            NotifyAuthenticationStateChanged(Task.FromResult(new AuthenticationState(new ClaimsPrincipal(new ClaimsIdentity()))));
        }


        private AuthenticationState GenAuthState(User user)
        {
            var identity = new ClaimsIdentity();
            if (user != null)
            {
                identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Role, user.Role)
                }, "apiauth_type");
            }
            return new AuthenticationState(new ClaimsPrincipal(identity));
        }
    }
}
