﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise;
using Blazorise.Sidebar;
using LoggingService.Features;
using LogsMonitor.Components.Client;
using LogsMonitor.Components.Dashboard;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.DashboardFeature;
using LogsMonitor.Pages.Client;
using LogsMonitor.Utility;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using JoinClientModal = LogsMonitor.Components.Client.JoinClientModal;

namespace LogsMonitor.Shared
{
    public partial class MainLayout
    {
        [Inject] protected NavigationManager NavigationManager { get; set; }
        [Inject] private AuthenticationStateProvider AuthenticationStateProvider { get; set; }
        [Inject] private IRequestHandler<GetAllClientsRequest, IList<Client>> GetAllClientsRequestHandler { get; set; }
        [Inject] private IRequestHandler<GetAllDashboardsRequest, IList<Dashboard>> GetAllDashboardsRequestHandler { get; set; }

        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }

        Sidebar sidebar;
        private SidebarInfo sidebarInfo;

        private JoinClientModal joinClientModal = new();
        private CreateClientModal createClientModal = new();
        private CreateDashboardModal createDashboardModal = new();

        private IList<Client> _clients = new List<Client>();
        private string userId;
        private IDictionary<Dashboard, Client> _dashboards;

        protected override async Task OnInitializedAsync()
        {
            var authState = await AuthenticationStateTask;
            if (authState?.User.Identity != null || authState.User.Identity.IsAuthenticated)
            {
                userId = authState.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                if (!string.IsNullOrEmpty(userId))
                {
                    _clients = await GetAllClientsRequestHandler.HandleRequest(new GetAllClientsRequest { UserId = userId, Active = true});
                    await GetDashboards();
                }
            }

            var items = new List<SidebarItemInfo>();
            foreach (var client in _clients)
            {
                var clientSideBar = new SidebarItemInfo
                {
                    Text = client.Name,
                    Icon = IconName.User,
                    SubItems = new List<SidebarItemInfo>
                    {
                        new SidebarItemInfo { To = $"Logs/{client.Id}", Text = "Raw Logs", Icon = IconName.GripLines},
                    }
                };

                var clientDashboards = await GetAllDashboardsRequestHandler.HandleRequest(new GetAllDashboardsRequest{ClientId = client.Id});
                foreach (var clientDashboard in clientDashboards)
                {
                    clientSideBar.SubItems.Add(new SidebarItemInfo
                    {
                        To = $"Dashboard/{client.Id}/{clientDashboard.Id}",
                        Text = $" {clientDashboard.Name}",
                        Icon = IconName.ChartPie
                    });
                    
                }
                items.Add(clientSideBar);
            }

            sidebarInfo = new SidebarInfo
            {
                Brand = new SidebarBrandInfo
                {
                    Text = "logMaster;"
                },
                Items = items
            };


            await base.OnInitializedAsync();
        }

        void ToggleSidebar()
        {
            sidebar.Toggle();
        }

        public void JoinClientClicked()
        {
            joinClientModal.ShowModel();
        }

        public void CreateClientClicked()
        {
            createClientModal.ShowModel();
        }

        public void CreateDashboardClicked()
        {
            createDashboardModal.ShowModal();
        }
        public void Logout()
        {
            ((CustomAuthenticationStateProvider)AuthenticationStateProvider).Logout();
        }

        public async Task GetDashboards()
        {
            _dashboards = new Dictionary<Dashboard, Client>();
            foreach (var client in _clients)
            {
                var dashboards = await GetAllDashboardsRequestHandler.HandleRequest(new GetAllDashboardsRequest { ClientId = client.Id });
                foreach (var dashboard in dashboards)
                {
                    _dashboards.Add(new KeyValuePair<Dashboard, Client>(dashboard, client));
                }
            }
        }
        public void NavigateToDashboard(string dashboardId, string clientId)
        {
            NavigationManager.NavigateTo($"/Dashboard/{clientId}/{dashboardId}");
        }
    }
}
