﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Models.Dashboard
{
    public class DashboardModel
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public bool ShowTotalLogs { get; set; }
        public bool ShowErrors { get; set; }
        public bool ShowWarnings { get; set; }
        public bool ShowInformation { get; set; }
        public XAxisTimeScale TimeScale { get; set; }
        public string LogIdentifier { get; set; } = string.Empty;
        public bool LogIdentifierExact { get; set; }
        public bool IsAdminOwner { get; set; }
        public bool Popup { get; set; }
        public int NumberToTrigger { get; set; }
        public int NumberActually { get; set; }
    }
}
