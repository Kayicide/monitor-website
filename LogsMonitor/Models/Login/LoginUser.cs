﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LogsMonitor.Models.Login
{
    public class LoginUser
    {
        [Required(ErrorMessage = "Email is required!")]
        [EmailAddress(ErrorMessage = "Invalid Email!")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Password is required!")]
        [PasswordPropertyText]
        public string Password { get; set; }
    }
}
