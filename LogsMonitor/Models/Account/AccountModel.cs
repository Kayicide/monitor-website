﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Models.Account
{
    public class AccountModel
    {
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string CurrentPassword { get; set; }
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Password Must be a Minimum of 6 and a Maximum of 30 Characters!")]
        public string NewPassword { get; set; }
        [Compare(nameof(NewPassword), ErrorMessage = "Passwords Must Match!")]
        public string ConfirmNewPassword { get; set; }

    }
}
