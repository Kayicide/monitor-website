﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Models.Logs
{
    public class LogsModel
    {
        public IList<Log> Logs { get; set; }
        public IList<Error> Errors { get; set; }
        public IList<Information> InformationLogs { get; set; }
        public IList<Warning> WarningLogs { get; set; }


        public int LogType { get; set; } = 1;
        //normal log filtering;
        public DateTime LowerBoundDateTime { get; set; } = DateTime.Now.AddDays(-1);
        public DateTime UpperBoundDateTime { get; set; } = DateTime.Now;
        public string Identifier { get; set; } = string.Empty;
        public bool IdentifierExact { get; set; } = false;
        public string Summary { get; set; } = string.Empty;
        public bool SummaryExact { get; set; } = false;
        //error logs
        public int Severity { get; set; } = 0;
        public string StackTrace { get; set; } = string.Empty;
        public bool StackTraceExact { get; set; } = false;
        //information logs
        public string InfoText { get; set; } = string.Empty;
        public bool InfoTextExact { get; set; } = false;
        //warning logs
        public string WarningText { get; set; } = string.Empty;
        public bool WarningTextExact { get; set; } = false;

    }
}
