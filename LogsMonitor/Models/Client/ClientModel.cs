﻿
using System.Collections.Generic;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Models.Client
{
    public class ClientModel
    {
        public Data.Entity.Client Client { get; set; }
        public IList<User> Members { get; set; }
        public bool Admin { get; set; }
        public bool ReadOnly => !Admin;
    }
}
