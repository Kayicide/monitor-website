﻿using System.ComponentModel.DataAnnotations;

namespace LogsMonitor.Models.Register
{
    public class RegisterUser
    {
        [Required]
        [EmailAddress(ErrorMessage = "Email Must be Valid!")]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [StringLength(6, ErrorMessage = "Password Must be a Minimum of 6 Characters!")]
        public string Password { get; set; }
        [Compare(nameof(Password), ErrorMessage = "Passwords Must Match!")]
        public string ConfirmPassword { get; set; }

    }
}
