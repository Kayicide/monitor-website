﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogsMonitor.Data.Entity;

namespace LogsMonitor.Models.Admin
{
    public class ManageClientModel
    {
        public Data.Entity.Client Client { get; set; }
        public IList<User> Admin { get; set; }
        public User Owner { get; set; }

    }
}
