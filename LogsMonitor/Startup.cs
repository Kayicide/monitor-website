using LogsMonitor.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Blazored.LocalStorage;
using Blazored.SessionStorage;
using LoggingService.Features;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features.UserFeature;
using LogsMonitor.Utility;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.IdentityModel.Tokens;
using SimpleInjector;
using Blazorise;
using Blazorise.Bootstrap;
using Blazorise.Icons.FontAwesome;
using LogsMonitor.Features;
using LogsMonitor.Features.AlertFeature;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.DashboardFeature;
using LogsMonitor.Features.EmailConfirmationFeature;
using LogsMonitor.Features.LogFeature;
using LogsMonitor.Features.Shared;
using LogsMonitor.Models.Dashboard;
using LogsMonitor.Utility.Mappers;
using Microsoft.Extensions.Options;
using Alert = LogsMonitor.Data.Entity.Alert;

namespace LogsMonitor
{
    public class Startup
    {
        private SimpleInjector.Container _container = new();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            _container.Options.ResolveUnregisteredConcreteTypes = false;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddBlazorise(options =>
                {
                    options.ChangeTextOnKeyPress = true;
                })
                .AddBootstrapProviders()
                .AddFontAwesomeIcons();

            services.AddRazorPages();
            services.AddServerSideBlazor();

            InitializeContainer();

            services.AddBlazoredSessionStorage();
            services.AddBlazoredLocalStorage();

            services.AddHttpClient();
            services.AddHttpClient("AuthService", c =>
            {
                c.BaseAddress = new Uri(Configuration.GetValue<string>("AuthService"));
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("Authorization", "Bearer " + GenToken(Configuration.GetValue<string>("AuthServiceSecret")));
            });
            services.AddHttpClient("LoggingService", c =>
            {
                c.BaseAddress = new Uri(Configuration.GetValue<string>("LoggingService"));
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("Authorization", "Bearer " + GenTokenForLogging(Configuration.GetValue<string>("LoggingServiceSecret")));
            });
            services.AddHttpClient("DashService", c =>
            {
                c.BaseAddress = new Uri(Configuration.GetValue<string>("DashService"));
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                c.DefaultRequestHeaders.Add("Authorization", "Bearer " + GenToken(Configuration.GetValue<string>("DashServiceSecret")));
            });

            services.Configure<Secrets>(x =>
            {
                x.LoggingServiceSecret = Configuration.GetValue<string>("LoggingServiceSecret");
                x.AuthServiceSecret = Configuration.GetValue<string>("AuthServiceSecret");
            });
            services.AddSingleton(x => x.GetRequiredService<IOptions<Secrets>>().Value);

            services.AddSingleton<IMapper<Dashboard, DashboardModel>, DashboardMapper>();

            services.AddScoped<AuthenticationStateProvider, CustomAuthenticationStateProvider>();
            services.AddTransient<IRequestHandler<RegisterUserRequest, User>, RegisterUserRequestHandler>();
            services.AddTransient<IRequestHandler<ConfirmEmailRequest, bool>, ConfirmEmailRequestHandler>();
            services.AddTransient<IRequestHandler<IsEmailConfirmedRequest, bool>, IsEmailConfirmedRequestHandler>();
            services.AddTransient<IRequestHandler<GetUserRequest, User>, GetUserRequestHandler>();
            services.AddTransient<IRequestHandler<UpdateUserRequest, User>, UpdateUserRequestHandler>();
            services.AddTransient<IRequestHandler<JoinClientRequest, bool>, JoinClientRequestHandler>();
            services.AddTransient<IRequestHandler<GetClientRequest, Client>, GetClientRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllClientsRequest, IList<Client>>, GetAllClientsRequestHandler>();
            services.AddTransient<IRequestHandler<CreateClientRequest, Client>, CreateClientRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllUsersRequest, IList<User>>, GetAllUsersRequestHandler>();
            services.AddTransient<IRequestHandler<UpdateClientNameRequest, Client>, UpdateClientNameRequestHandler>();
            services.AddTransient<IRequestHandler<RegenJoinTokenRequest, Client>, RegenJoinTokenRequestHandler>();
            services.AddTransient<IRequestHandler<GenerateJwtTokenRequest, Client>, GenerateJwtTokenRequestHandler>();
            services.AddTransient<IRequestHandler<LeaveClientRequest, bool>, LeaveClientRequestHandler>();
            services.AddTransient<IRequestHandler<AddAdminRequest, Client>, AddAdminRequestHandler>();
            services.AddTransient<IRequestHandler<RemoveAdminRequest, Client>, RemoveAdminRequestHandler>();
            services.AddTransient<IRequestHandler<TransferOwnershipRequest, Client>, TransferOwnershipRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllLogsRequest, IList<Log>>, GetAllLogsRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllErrorLogsRequest, IList<Error>>, GetAllErrorLogsRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllInformationLogsRequest, IList<Information>>, GetAllInformationLogsRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllWarningLogsRequest, IList<Warning>>, GetAllWarningLogsRequestHandler>();
            services.AddTransient<IRequestHandler<CreateDashboardRequest, Dashboard>, CreateDashboardRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllDashboardsRequest, IList<Dashboard>>, GetAllDashboardsRequestHandler>();
            services.AddTransient<IRequestHandler<CreateDashboardRequest, Dashboard>, CreateDashboardRequestHandler>();
            services.AddTransient<IRequestHandler<GetDashboardRequest, Dashboard>, GetDashboardRequestHandler>();
            services.AddTransient<IRequestHandler<UpdateDashboardRequest, Dashboard>, UpdateDashboardRequestHandler>();
            services.AddTransient<IRequestHandler<GetAllAlertsRequest, IList<Alert>>, GetAllAlertsRequestHandler>();
            services.AddTransient<IRequestHandler<CreateNewAlertRequest, Alert>, CreateNewAlertRequestHandler>();
            services.AddTransient<IRequestHandler<UpdateAlertRequest, Alert>, UpdateAlertRequestHandler>();
        }

        private void InitializeContainer()
        {
            _container.Register(typeof(IRequestHandler<,>), typeof(IRequestHandler<,>).Assembly, Lifestyle.Transient);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }


        private string GenTokenForLogging(string secret)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.UniqueName, "FrontEndJWT"),
                    new Claim(ClaimTypes.Role, "FrontEnd")
                }),
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        private string GenToken(string secret)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.UtcNow.AddYears(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}

