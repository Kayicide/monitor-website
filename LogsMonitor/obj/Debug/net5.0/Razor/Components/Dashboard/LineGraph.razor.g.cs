#pragma checksum "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\Components\Dashboard\LineGraph.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9a95bbd899ea563a39435c50e1de86e3a0caad75"
// <auto-generated/>
#pragma warning disable 1591
namespace LogsMonitor.Components.Dashboard
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Blazorise;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Blazorise.DataGrid;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Blazorise.Sidebar;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor.Components.Dashboard;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor.Components.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor.Components.Logs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using LogsMonitor.Pages.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\_Imports.razor"
using Blazorise.Charts;

#line default
#line hidden
#nullable disable
    public partial class LineGraph : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Blazorise.Charts.LineChart<int>>(0);
            __builder.AddAttribute(1, "Options", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Blazorise.Charts.LineChartOptions>(
#nullable restore
#line 2 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\Components\Dashboard\LineGraph.razor"
                                                 new LineChartOptions()

#line default
#line hidden
#nullable disable
            ));
            __builder.AddComponentReferenceCapture(2, (__value) => {
#nullable restore
#line 2 "C:\Users\Scarlett\Documents\FYP\MonitorWebsite\LogsMonitor\Components\Dashboard\LineGraph.razor"
                 lineChart = (Blazorise.Charts.LineChart<int>)__value;

#line default
#line hidden
#nullable disable
            }
            );
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
