﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Components.Client
{
    public partial class CreateClientModal
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }
        [Inject] private IRequestHandler<CreateClientRequest, Data.Entity.Client> CreateClientRequestHandler { get; set; }
        [Inject] private IRequestHandler<GenerateJwtTokenRequest, Data.Entity.Client> GenerateJwtTokenRequestHHandler { get; set; }

        private Modal modalRef;
        [Required]
        private string _name = "";
        private string _error = "";
        private string _currentUser;
        protected override async Task OnInitializedAsync()
        {
            _currentUser = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        }

        public async Task CreateClient()
        {
            if (!string.IsNullOrEmpty(_name))
            {
                var client = await CreateClientRequestHandler.HandleRequest(new CreateClientRequest { UserId = _currentUser, Name = _name, });
                if (client == null)
                {
                    _error = "Error Creating Team! Please Contact Support!";
                }
                else
                {
                    await GenerateJwtTokenRequestHHandler.HandleRequest(new GenerateJwtTokenRequest { ClientId = client.Id, CurrentUser = _currentUser });
                    HideModal();
                    NavigationManager.NavigateTo($"/Team/{client.Id}", true);
                }
            }
            else
            {
                _error = "Name Cannot be Empty!";
            }
        }
        public void HideModal()
        {
            modalRef.Hide();
        }
        public void ShowModel()
        {
            modalRef.Show();
        }
    }
}
