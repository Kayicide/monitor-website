﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.UserFeature;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Components.Client
{
    public partial class JoinClientModal
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Inject] private NavigationManager NavigationManager { get; set; }
        [Inject] private IRequestHandler<JoinClientRequest, bool> JoinClientRequestHandler { get; set; }
        [Inject] private IRequestHandler<GetAllClientsRequest, IList<Data.Entity.Client>> GetAllClientsRequestHandler { get; set; }

        private Modal modalRef;
        private string _joinCode;
        private string _error = "";
        private string _currentUser;

        protected override async Task OnInitializedAsync()
        {
            _currentUser = (await AuthenticationStateTask).User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
        }

        public async void JoinClient()
        {
            var result = await JoinClientRequestHandler.HandleRequest(new JoinClientRequest{ClientJoinToken = _joinCode, CurrentUser = _currentUser});
            if (!result)
                _error = "Join Code is not Valid!";
            
            var client = (await GetAllClientsRequestHandler.HandleRequest(new GetAllClientsRequest{JoinToken = _joinCode})).FirstOrDefault();
            if (client == null)
            {
                _error = "Error occurred while trying to navigate to teams page! Contact support!";
            }
            else
            {
                HideModal();
                NavigationManager.NavigateTo($"Team/{client.Id}", true);
            }
        }

        public void HideModal()
        {
            modalRef.Hide();
        }
        public void ShowModel()
        {
            modalRef.Show();
        }
    }
}
