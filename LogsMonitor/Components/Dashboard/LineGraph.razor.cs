﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using AngleSharp.Css;
using Blazorise.Charts;
using LogsMonitor.Data.Entity;
using LogsMonitor.Features;
using LogsMonitor.Features.DashboardFeature;
using LogsMonitor.Features.LogFeature;
using Microsoft.AspNetCore.Components;

namespace LogsMonitor.Components.Dashboard
{
    public partial class LineGraph
    {
        [Inject] public IRequestHandler<GetAllLogsRequest, IList<Log>> GetAllLogsRequestHandler { get; set; }
        [Inject] public IRequestHandler<GetDashboardRequest, Data.Entity.Dashboard> GetDashboardRequestHandler { get; set; }
        [Parameter] public string ClientId { get; set; }
        [Parameter] public string DashboardId { get; set; }
        private Data.Entity.Dashboard _settings { get; set; }
        private IList<Log> Logs { get; set; }
        LineChart<int> lineChart = new();


        protected override async Task OnParametersSetAsync()
        {
            await HandleRedraw();
        }
        public async Task HandleRedraw()
        {
            _settings = await GetDashboardRequestHandler.HandleRequest(new GetDashboardRequest { DashboardId = DashboardId });
            Logs = await GetAllLogsRequestHandler.HandleRequest(new GetAllLogsRequest
                { ClientId = ClientId, LowerBoundDateTime = GetDateTime(), Identifier = _settings.LogIdentifier, IdentifierExact = _settings.LogIdentifierExact});

            await lineChart.Clear();
            var dataSets = new List<LineChartDataset<int>>();

            if (_settings.ShowTotalLogs)
                dataSets.Add(GetAllLogsDataset());
            if (_settings.ShowErrors)
                dataSets.Add(GetAllErrorsDataset());
            if (_settings.ShowWarnings)
                dataSets.Add(GetAllWarningsDataset());
            if (_settings.ShowInformation)
                dataSets.Add(GetAllInformationsDataset());

            await lineChart.AddLabelsDatasetsAndUpdate(GetBottomLabels(), dataSets.ToArray());
        }

        private LineChartDataset<int> GetAllLogsDataset()
        {
            return new LineChartDataset<int>
            {
                Label = "Total Logs",
                Data = SplitList(Logs),
                BorderColor = new List<string>
                {
                    ChartColor.FromRgba(41, 43, 44, 1f), ChartColor.FromRgba(41, 43, 44, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(91, 192, 222, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(91, 192, 222, 1f)
                },
                Fill = false,
                PointRadius = 2,
                BorderDash = new List<int> { }
            };
        }

        private LineChartDataset<int> GetAllErrorsDataset()
        {
            return new()
            {
                Label = "Error Logs",
                Data = SplitList(Logs.Where(x => x.GetType() == typeof(Error)).ToList()),
                BorderColor = new List<string>
                {
                    ChartColor.FromRgba(217, 83, 79, 1f), ChartColor.FromRgba(0,0,0, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(0,0,0, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(0,0,0, 1f)
                },
                Fill = false,
                PointRadius = 2,
                BorderDash = new List<int> { }
            };
        }

        private LineChartDataset<int> GetAllWarningsDataset()
        {
            return new()
            {
                Label = "Warning Logs",
                Data = SplitList(Logs.Where(x => x.GetType() == typeof(Warning)).ToList()),
                BorderColor = new List<string>
                {
                    ChartColor.FromRgba(240, 173, 78, 1f), ChartColor.FromRgba(240, 173, 78, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(91, 192, 222, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(91, 192, 222, 1f)
                },
                Fill = false,
                PointRadius = 2,
                BorderDash = new List<int> { }
            };
        }

        private LineChartDataset<int> GetAllInformationsDataset()
        {
            return new()
            {
                Label = "Information Logs",
                Data = SplitList(Logs.Where(x => x.GetType() == typeof(Information)).ToList()),
                BorderColor = new List<string>
                {
                    ChartColor.FromRgba(91, 192, 222, 1f), ChartColor.FromRgba(91, 192, 222, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(91, 192, 222, 1f),
                    ChartColor.FromRgba(0,0,0, 1f), ChartColor.FromRgba(91, 192, 222, 1f)
                },
                Fill = false,
                PointRadius = 2,
                BorderDash = new List<int> { }
            };
        }
        private string[] GetBottomLabels()
        {
            switch (_settings.TimeScale)
            {
                case XAxisTimeScale.Day:
                    return Intervals().Select(x => x.ToString("d")).ToArray();
                case XAxisTimeScale.Minute:
                    return Intervals().Select(x => x.ToString("t")).ToArray();
                default:
                    return Intervals().Select(x => x.ToString("g")).ToArray();
            }
        }


        private IList<DateTime> Intervals()
        {
            var date = GetDateTime();
            if (_settings.TimeScale == XAxisTimeScale.Day)
            {

                return Enumerable.Range(0, 1 + (int)DateTime.Now.Subtract(date).TotalDays)
                    .Select(offset => date.AddDays(offset).AddHours(-date.Hour).AddMinutes(-date.Minute).AddSeconds(-date.Second))
                    .ToList();
            }
            if (_settings.TimeScale == XAxisTimeScale.Minute)
            {
                return Enumerable.Range(0, 1 + (int)DateTime.Now.Subtract(date).TotalMinutes)
                    .Select(offset => date.AddMinutes(offset).AddSeconds(-date.Second))
                    .ToList();
            }
            //Hour
            return Enumerable.Range(0, 1 + (int)DateTime.Now.Subtract(date).TotalHours)
                .Select(offset => date.AddHours(offset).AddMinutes(-date.Minute).AddSeconds(-date.Second))
                .ToList();
        }

        private DateTime GetDateTime()
        {
            switch (_settings.TimeScale)
            {
                case XAxisTimeScale.Day:
                    return DateTime.Now.AddDays(-7);
                case XAxisTimeScale.Minute:
                    return DateTime.Now.AddHours(-1);
                default:
                    return DateTime.Now.AddDays(-1);
            }
        }

        private List<int> SplitList(IList<Log> logsToSplit)
        {
            var logsPerLabel = new List<int>();
            var dates = Intervals();

            if (_settings.TimeScale == XAxisTimeScale.Day)
            {
                foreach (var date in dates)
                {
                    var logsOnHour = logsToSplit.Count(x =>
                        x.DateTimeOfOccurrence > date && x.DateTimeOfOccurrence <= date.AddHours(23));
                    logsPerLabel.Add(logsOnHour);
                }
            }
            else if (_settings.TimeScale == XAxisTimeScale.Minute)
            {
                foreach (var date in dates)
                {
                    var logsOnHour = logsToSplit.Count(x =>
                        x.DateTimeOfOccurrence > date && x.DateTimeOfOccurrence <= date.AddSeconds(59));
                    logsPerLabel.Add(logsOnHour);
                }
            }
            else
            {
                //Hour
                foreach (var date in dates)
                {
                    var logsOnHour = logsToSplit.Count(x =>
                        x.DateTimeOfOccurrence > date && x.DateTimeOfOccurrence <= date.AddMinutes(59));
                    logsPerLabel.Add(logsOnHour);
                }
            }

            return logsPerLabel;
        }

        public async Task Refresh(DateTime dateTime, IList<Log> newLogs)
        {
            var index = 0;
            var label = NewLabel(dateTime);
            var labelAdded = !string.IsNullOrEmpty(label);
            if (_settings.ShowTotalLogs)
            {
                await UpdateDataSet(index, labelAdded, newLogs);
                index++;
            }
            if (_settings.ShowErrors)
            {
                await UpdateDataSet(index, labelAdded, newLogs.Where(x => x.GetType() == typeof(Error)).ToList());
                index++;
            }
            if (_settings.ShowWarnings)
            {
                await UpdateDataSet(index, labelAdded, newLogs.Where(x => x.GetType() == typeof(Warning)).ToList());
                index++;
            }
            if (_settings.ShowInformation)
            {
                await UpdateDataSet(index, labelAdded, newLogs.Where(x => x.GetType() == typeof(Information)).ToList());
            }

            if(labelAdded)
                await UpdateLabel(label);
        }

        private async Task UpdateDataSet(int index, bool labelAdded, IList<Log> logs)
        {
            await InvokeAsync(async () =>
            {
                if (lineChart.Data.Datasets[index] != null)
                {
                    if (logs.Count != lineChart.Data.Datasets[index].Data[^1])
                    {
                        await lineChart.PopData(index);
                        await lineChart.AddData(index, logs.Count);
                    }
                    if (labelAdded)
                    {
                        await lineChart.AddData(index, 0);
                        await lineChart.ShiftData(index);
                    }
                }
                StateHasChanged();
            });
        }

        private async Task UpdateLabel(string label)
        {
            await InvokeAsync(async () =>
            {
                await lineChart.AddLabels(label);
                await lineChart.ShiftLabel();
                StateHasChanged();
            });
        }
        private string NewLabel(DateTime dateTime)
        {
            switch (_settings.TimeScale)
            {
                case XAxisTimeScale.Day:
                    if (dateTime.Hour == 0 && dateTime.Minute == 1)
                        return dateTime.ToString("d");
                    break;
                case XAxisTimeScale.Minute:
                    return dateTime.ToString("t");
                default:
                    if (dateTime.Minute == 1)
                        return dateTime.ToString("g");
                    break;
            }
            return "";
        }

    }
}
