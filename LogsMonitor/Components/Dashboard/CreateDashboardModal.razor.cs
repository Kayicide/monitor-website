﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Blazorise;
using LogsMonitor.Features;
using LogsMonitor.Features.ClientFeature;
using LogsMonitor.Features.DashboardFeature;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;

namespace LogsMonitor.Components.Dashboard
{
    public partial class CreateDashboardModal
    {
        [CascadingParameter] private Task<AuthenticationState> AuthenticationStateTask { get; set; }
        [Inject] protected NavigationManager NavigationManager { get; set; }
        [Inject] protected IRequestHandler<CreateDashboardRequest, Data.Entity.Dashboard> CreateDashboardRequestHandler { get; set; }
        [Inject] private IRequestHandler<GetAllClientsRequest, IList<Data.Entity.Client>> GetAllClientsRequestHandler { get; set; }
        private Modal _modalRef;
        private string _clientId;
        private string _name;
        private IList<Data.Entity.Client> _clients;
        protected override async Task OnInitializedAsync()
        {
            var authState = await AuthenticationStateTask;
            if (authState?.User.Identity != null || authState.User.Identity.IsAuthenticated)
            {
                var userId = authState.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                if (!string.IsNullOrEmpty(userId))
                {
                    _clients = (await GetAllClientsRequestHandler.HandleRequest(new GetAllClientsRequest { UserId = userId, Active = true })).Where(x => x.Admins.Contains(userId)).ToList();
                    _clientId = _clients.FirstOrDefault()?.Id;
                }
                
            }
        }

        private async Task Create()
        {
            var dashboard = await CreateDashboardRequestHandler.HandleRequest(new CreateDashboardRequest{ClientId = _clientId, Name = _name});
            _modalRef.Hide();
            NavigationManager.NavigateTo($"Dashboard/{_clientId}/{dashboard.Id}");
        }

        private void HideModal()
        {
            _modalRef.Hide();
        }

        public void ShowModal()
        {
            _modalRef.Show();
        }
    }
}
